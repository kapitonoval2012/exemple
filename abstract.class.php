<?php


/**
 * генерирует стандартный ответ при вызове AJAX метода
 * содержит конструктор оповещений и окон.
 *
 * @return    JSON - response
 * @author    Алексей Капитонов
 * @version    12:15 22.12.2015
 */
class responseClass extends aplStdAJAXMethod
{

    function __construct()
    {
    }

    /**
     * Добавляет POST-окно в addResponseFunction ( ui - окно ) присутствует кнопка "OK"
     *
     * @param string $wText
     * @param string $wTitle
     * @param array $options
     */
    public function addPostWindow($wText = '', $wTitle = 'Имя POST-окна', $options = array())
    {
        $this->response['response'] = 'show_new_window';
        $this->response['title'] = $wTitle;

        if (isset($options['width'])) {
            $this->response['width'] = $options['width'];
        }
        if (isset($options['height'])) {
            $this->response['height'] = $options['height'];
        }
        if (isset($options['button_name'])) {
            $this->response['button_name'] = $options['button_name'];
        }

        if (isset($options['form_id'])) {
            $wHtml = '<form id="' . $options['form_id'] . '">';
        } else {
            $wHtml = '<form>';
        }

        if (isset($_POST)) {
            unset($_POST['AJAX']);
            foreach ($_POST as $key => $value) {
                $wHtml .= '<input type="hidden" value="' . $value . '" name="' . $key . '">';
            }
        }
        $wHtml .= $wText;
        $wHtml .= "</form>";

        $this->response['html'] = base64_encode($wHtml);
    }

    /**
     * Добавляет Simple-окно в addResponseFunction ( ui - окно ) присутствует кнопка "закрыть"
     *
     * @param string $wText
     * @param string $wTitle
     * @param array $options
     */
    public function addSimpleWindow($wText = '', $wTitle = 'Имя Simple-окна', $options = array())
    {
        $this->response['response'] = 'show_new_window_simple';
        $this->response['title'] = $wTitle;

        if (isset($options['width'])) {
            $this->response['width'] = $options['width'];
        }
        if (isset($options['height'])) {
            $this->response['height'] = $options['height'];
        }


        $wHtml = '<form>';
        if (isset($_POST)) {
            unset($_POST['AJAX']);
            $wHtml .= $this->addValEach($_POST, '');
        }
        $wHtml .= $wText;
        $wHtml .= "</form>";

        $this->response['html'] = base64_encode($wHtml);
    }

    /**
     * @param $arr
     * @param $html
     * @return string
     */
    function addValEach($arr, $html)
    {
        foreach ($arr as $key => $value) {

            if (is_array($value)) {
                $html .= $this->addValEach($value, $html);
                continue;
            }
            $html .= '<input type="hidden" value="' . $value . '" name="' . $key . '">';
        }
        return $html;
    }

    /**
     * Добавляет собщение в addResponseFunction
     *
     * @param string $mText
     * @param string $mType
     * @param int $time
     */
    public function addMessage($mText = '', $mType = 'error_message', $time = 0)
    {
        $function = 'echo_message';
        if ($time > 0) {
            $options['time'] = $time;
        }
        $options['message'] = $mText;
        $options['message_type'] = $mType;

        $this->addResponseFunction($function, $options);
    }


    /**
     * отдает ответ сервера на AJAX запрос
     *
     * @return string
     */
    public function getResponse()
    {
        if (!isset($this->response['response'])) {
            $response['response'] = "OK";
        } else {
            $response = $this->response;
        }

        //передача дополнительных даннных
        if (isset($this->response['data'])) {
            $response['data'] = $this->response['data'];
        }
        //передача дополнительных даннных
        if (isset($this->response['errors'])) {
            $response['errors'] = $this->response['errors'];
        }

        // добавляем функции JS
        if (count($this->response_functions)) {
            $response['function'] = $this->response_functions;
        }
        // добавляем опции
        if (count($this->options)) {
            $response = array_merge($response, $this->options);
        }

        return json_encode($response);
    }

    /**
     * добавляет функцию JS в список функций,
     * которые будут запрошены в JS когда будет получен ответ от сервера
     *
     * @param $function -   - имя функции JS, $options - опции для работы данной функции
     * @param array $options
     */
    public function addResponseFunction($function, $options = array())
    {
        $i = count($this->response_functions);

        // наименование функции
        $this->response_functions[$i]['function'] = $function;

        // текст всегда передается в base64
        foreach ($options as $key => $option) {
            switch ($key) {
                case 'html':
                    $this->response_functions[$i][$key] = base64_encode($option);
                    break;

                case 'message':
                    $this->response_functions[$i][$key] = base64_encode($option);
                    break;

                case 'text':
                    $this->response_functions[$i][$key] = base64_encode($option);
                    break;

                default:
                    // остальное как есть
                    $this->response_functions[$i][$key] = $option;
                    break;
            }
        }
    }

    /**
     * добавляет дополнительные опции,
     * которые будут запрошены в JS когда будет получен ответ от сервера
     *
     * @param array $options
     */
    public function addResponseOptions($options = array())
    {
        if (count($options)) {
            $this->options = array_merge($options, $this->options);
        }
    }
}
