from grab import Grab


class Parser(object):
    headers = {
        'Accept': 'application/xml'
    }
    cookies = {
        'secureid': '234287a68s7df8asd6f'
    }

    """
        склады оазиса доступны тут:
        http://api.oasiscatalog.com/v3/warehouses

        но так как, они навряд ли будут часто меняться, а нам нужно четко залочить, что из РФ, а что из европы
        задаем их в объекту явно, если перестет работать, будем смотреть и поправлять id
        в будущем м.б. придумаем что-то получше
    """

    authorisation_key = 'key2e12faed3d4c447ba8e6172a8d16b9da'
    protocol = 'http://'
    urlSearch = 'api.oasiscatalog.com/v3/products?articles='

    # т.к. сначала мы будем запрашивать сами артикулы и путентификация будет пройдена там, в запросе ключ не используется
    urlStock = 'http://key2e12faed3d4c447ba8e6172a8d16b9da:@api.oasiscatalog.com/v3/stock?format=xml&articles='
    # артикулы, по которым будем происводить запрос по остаткам
    artsForStock = []

    def get_artsForStock(self):
        return self.artsForStock

    def add_artsForStock(self, art=''):
        if art != '':
            self.artsForStock.append(art)

    errors = []
    article = []
    timeSleep = 0.10

    def format_errors(self, name='Название ошибки', text="Подобная информация об ошибке"):
        """
        формализует информацию об ошибках к единому виду, установленному в качестве стандарта обмена сообщениями от API к SRM Апельбурга
        :param name: название сообщения, может не попасть в сообщение
        :param text: сообщение, вся подробная информация содержится тут
        :return:
        """
        error = {
            'name': name,
            'text': text,

        }

        return error

    def get_article(self):
        return self.article

    def set_article(self, article):
        self.article = article

    def art_clean(self):
        """
        возвращает массив артикулов без окончаний размеров
        данные артикулы используются в качестве ключей для объекта возвращаемого клиенту
        :return:
        """
        returnArt = []
        for art in self.article:
            returnArt.append(art.split('-')[0])
        return returnArt

    def get_url_search(self):
        """
        сборка запроса к API поставщика
        :return:
        """
        # собираем артикулы в строку для отправки на API поставщика
        artSearchString = ','.join(self.get_article())
        # вырезаем #
        artSearchString = artSearchString.replace('-', '')
        # сборка URL
        query_url = self.protocol + self.authorisation_key + ':@' + self.urlSearch + artSearchString
        return query_url

    def __init__(self, art_array):
        """
        инициализация
        :param art_array:
        """
        self.set_article(art_array)

    def connect_to_supplier(self):
        """
        подключение к API
        запрос к API по товарам
        :return:
        """
        g = Grab()

        # настройки
        g.setup(headers=self.headers, cookies=self.cookies)

        # производим запрос
        g.go(self.get_url_search())

        return g

    def connect_to_stock_api(self, arts):
        """
        запрос остатков по артикулам
        :param arts:
        :return:
        """
        g = Grab()

        # настройки
        g.setup(headers=self.headers, cookies=self.cookies)

        # производим запрос
        g.go(self.urlStock + arts)

        return g

    def get_data(self):
        """
        парсит и возвращает данные по товарам
        :return:
        """
        # подключение к API
        g = self.connect_to_supplier()

        # если запрошен хотябы один артикул
        if len(self.get_article()) > 0:
            # объявляем пустой объект info
            info = {}
            # если в выдаче есть артикулы

            """
            TODO артикул приходит в виде                65465654#XL
            TODO отправлять к поставщику следует как:   65465654#XL
            TODO отправляеть назад как:                 65465654
            """

            # получаем массив артикулов без окончаний размеров
            searchArtClean = self.art_clean()

            # если если информация по запросу найдена будут выведены группы
            if g.doc.select('//group').count() > 0:
                # перебор групп
                for groupItem in g.doc.select('//group'):
                    # перебор артикулов в группе
                    for articleItem in groupItem.select('.//item'):

                        # если есть информация по артикулу
                        if (articleItem.select('.//article').count() > 0):

                            # т.к. поставщик по запрошенному артикулу вываливает нам все цвета и размеры как отдельные артикулы
                            # их необходимо проверять на соответствие с искомым артикулом
                            # проверяем соответствует ли данный артикул искомому

                            for searchArt in searchArtClean:
                                # если искомый артикул соответствует articleItem
                                if searchArt in articleItem.select('.//article')[0].text():
                                    # берём информацию по данному артикулу(размеру)
                                    info = self.get_info(connection=articleItem, art=searchArt, info=info)

            else:
                # обработка случая полного отсутствия информации по запрощенным артикулам
                return {
                    'errors': self.format_errors(name="Информация по артикулам не найдена. Интерпрезент.",
                                                 text="Артикул(ы) " + ','.join(
                                                     self.get_article()) + " отсутствует на сайте поставщика")
                }

            # добавляем информацию по остаткам
            info = self._get_stock_data(info=info)

            # возвращаем полученную информацию
            return info
        else:
            return {
                'errors': self.format_errors(name="Артикул не указан",
                                             text=self.get_url_search())
            }

    #
    def get_info(self, connection, art, info):
        """
        осуществлена работа с информацией в одном ITEM

        добавляет информацию по товару в общий массив ответа
        :param connection:  экземпляр выбрки Grab
        :param art:         артикул находящийся в ходу в апельбурге (без префикса 26)
        :param info:        общий массив ответа
        :return:
        """
        # если по данному артикулу мы ещё не собрали информации
        if not art in info:
            # собюираем общую информацию по артикулу
            response = {
                'article': art,
                # 'name': connection.select('.//name')[0].text(),
                # 'link': self.get_url_search(),
                'prices': self._get_price(connection=connection),
                'size': []
            }
            # добавляем информацию в общий объект
            info[art] = response

        # добавляем информауию по размеру
        info[art]['size'].append(self._get_size(connection=connection))

        return info

    def _get_price(self, connection):
        """
        возвращает информацию по ценам
        :param connection: экземпляр выбрки Grab
        :return:
        """
        price_info = {
            'price_in': connection.select('.//discount_price')[0].text(),
            'price_out': connection.select('.//price')[0].text()
        }

        return price_info

    def _get_size(self, connection):
        """
        возвращает информацию по размерам
        :param connection: экземпляр выбрки Grab
        :return:
        """
        if connection.select('.//size').count() > 0:
            # получаем название артикула хранящегося у поставщика
            size_name = connection.select('.//size')[0].text()
            article_name = connection.select('.//article')[0].text()
            # запоминаем артикул поставщика для запроса к API по остаткам
            self.add_artsForStock(art=article_name)

            size_data = {
                'size_name': connection.select('.//size')[0].text(),
                'product_id': connection.select('.//id')[0].text(),
                # 'prices': self._get_price(connection=connection),
            }

            return size_data

        return {}

    def _get_stock_data(self, info):
        """
        :получаем информацию по остаткам
        :param info:общий массив ответа
        :return:
        """

        # если были найдены артикулы поставщика

        if len(self.get_artsForStock()) > 0:
            # выполняем запрос к API по остаткам
            connection = self.connect_to_stock_api(arts=','.join(self.get_artsForStock()))

            # получаем данные по остаткам
            stock_data = self.parese_stock_data(connection=connection)

            # обрабатываем полученные данные
            info = self.stock_data_processing(stock_data=stock_data, info=info)

        return info

    def parese_stock_data(self, connection):
        """
        разбор полученной информации
        :return:
        """
        stock_data = {}

        # перебор  всех складов поставщика
        for item in connection.doc.select('//item'):
            # получаем данные по складскому остатку по конкретному складу
            stock_data = self.add_stock_data(item=item, stock_data=stock_data)

        return stock_data

    def add_stock_data(self, item, stock_data):
        """
        инкапсуляция обработки над объектом склада
        :param item:        объект склада
        :param stock_data:  данные по складу для данного артикула
        :return:
        """

        # список по умолчанию для пустого склада
        clean_data = {
            'in_stock_full': 0,
            'in_stock_now': 0,
            'reserve_now': 0,
            'gde': []
        }


        # id товара
        product_id = item.select('.//product_id')[0].text()
        # id склада
        destination_id = item.select('.//warehouse_id')[0].text()

        # склад + резерв
        amount = item.select('.//amount')[0].text()
        # резерв
        reserve = item.select('.//reserve')[0].text()
        # склад без резерва
        stock = item.select('.//stock')[0].text()
        # дата поставки
        delivery_date = item.select('.//delivery_date')[0].text()

        # определяем местоположение товара
        geografic_position = self.warehouse[destination_id]['destination']

        # если по данному товарау не сохранялась информация
        if not (product_id in stock_data):
            # создаем для товара пустой список
            stock_data[product_id] = {geografic_position: clean_data}
        else:
            # если по данному товару данного местоположения не было указано
            if not (geografic_position in stock_data[product_id]):
                # добавляем пустой список по данному местоположению
                stock_data[product_id][geografic_position] = clean_data

        # обновляем данные в общем списке
        stock_data[product_id][geografic_position] = self.stok_info(
            stock=stock,
            reserve=reserve,
            destination_id=destination_id,
            old_data=stock_data[product_id][geografic_position])

        return stock_data

    def stok_info(self, stock, reserve, destination_id, old_data):
        """
        форматирует данные об остатках возвращаемые клиенту
        :param in_stock_full:
        :param in_stock_now:
        :return:
        """

        # если на данном складе есть хоть что-нибудь
        if (int(stock) + int(reserve)) > 0:
            # добавляем добавляем информацию по складу
            gde_arr = {
                'destination': self.warehouse[destination_id]['name'],
                'destination_id': destination_id
            }
            old_data['gde'].append(gde_arr)

        # формируем ответ
        response_arr = {
            'in_stock_full': int(stock) + int(reserve) + int(old_data['in_stock_full']),
            'in_stock_now': int(stock) + int(old_data['in_stock_now']),
            'reserve_now': int(reserve) + int(old_data['reserve_now']),
            'gde': old_data['gde'],
        }

        return response_arr

    # данные расположения находятся в API
    warehouse = {
        '000000027': {
            'destination': 'simples',
            'name': 'Офис-Образцы СПб'
        },
        '000000029': {
            'destination': 'rf',
            'name': 'Оптовый МСК'
        },
        '000000034': {
            'destination': 'simples',
            'name': 'Офис - Образцы МСК'
        },
        '000000039': {
            'destination': 'europe',
            'name': 'Товары в пути'
        },
        '1-0000034': {
            'destination': 'rf',
            'name': 'VIP Полежаевская'
        },
        '1-0000052': {
            'destination': 'europe',
            'name': 'Удаленные(европа)'
        }
    }

    def stock_data_processing(self, stock_data, info):
        """
        обрабатывает данные полученные из API по остаткам
        :param connection:  экземпляр выбрки Grab
        :param info:        общий массив ответа
        :return:
        """

        # перебор позиций
        for key in info:
            i = 0
            #     # если в позиции содержится информация по размеру (а она должна быть обязательно)
            if info[key]['article'] != '':
                # перебираем все размеры
                for sizeInfo in info[key]['size']:
                    """
                        декомпозиция метода скорее запутает
                    """
                    # id продукта
                    product_id = sizeInfo['product_id']
                    # если в массиве данных по остаткам есть данные по данному id
                    if product_id in stock_data:
                        # добавление данных по остаткам в общий массив данных
                        info[info[key]['article']]['size'][i]['stock'] = stock_data[product_id]
                        i += 1

        return info